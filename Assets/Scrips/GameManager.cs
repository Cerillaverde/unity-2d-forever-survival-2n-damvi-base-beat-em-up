using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance = null;

    [HideInInspector]
    public int level;
    [HideInInspector]
    public int playerSelected;
    [HideInInspector]
    public int coins;

    public audioBGManager audioBGManager;
    public audioSFXManager audioSFXManager;
    [SerializeField] private SOGeneralStats SOPersistent;

    [Space(10)]
    [Header("Level Canvas")]
    [SerializeField] private GameObject levelCanvas;
    [SerializeField] private TMP_Text enemiesInCanvas;
    [SerializeField] private TMP_Text playerStats;
    [SerializeField] private TMP_Text coinsCanvas;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private TMP_Text CountDownCanvas;

    [Space(10)]
    [Header("Menu Canvas")]
    [SerializeField] private GameObject menuCanvas;
    [SerializeField] private UnityEngine.UI.Image characterSelect;
    [SerializeField] private AnimatorController[] animators;

    [Space(10)]
    [Header("Menu GameOver")]
    [SerializeField] private GameObject GameOverCanvas;

    [Space(10)]
    [Header("Menu Options")]
    [SerializeField] private GameObject optionsCanvas;

    private bool gameIsPaused = false;
    public bool gamePause => gameIsPaused;

    private playerController m_playerController;
    public playerController GetPlayerController() { return m_playerController; }


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        levelCanvas.SetActive(false);
        GameOverCanvas.SetActive(false);
        optionsCanvas.SetActive(false);

        //SO START
        coins = SOPersistent.coins;
        playerSelected = 0;
        audioBGManager.setVolume();
        audioSFXManager.setVolume();

        level = 1;

        LoadMenuScene();
        characterSelect.GetComponent<Animator>().runtimeAnimatorController = animators[0];

    }

    // CHARACTER SELECTION
    public void RightCharacter()
    {
        playerSelected++;
        if (playerSelected > 3)
        {
            playerSelected = 0;
        }
        characterSelect.GetComponent<Animator>().runtimeAnimatorController = animators[playerSelected];
    }
    public void LeftCharacter()
    {
        playerSelected--;
        //playerSelected = (playerSelected - 1) % animators.Length;
        if (playerSelected < 0)
        {
            playerSelected = 3;
        }
        characterSelect.GetComponent<Animator>().runtimeAnimatorController = animators[playerSelected];
    }

    // PLAYER FUNCTIONS
    internal void SetPlayer(playerController playerController)
    {
        m_playerController = playerController;
    }

    public void DestoyPlayer()
    {
        Destroy(m_playerController);
    }

    //SCENE MANAGER
    public void LoadLevelScene()
    {
        SceneManager.LoadScene("LevelScene");
        IntoLevelScene();
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
        IntoMenuScene();
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene("GameOver");
        IntoGameOverScene();
    }

    public void IntoLevelScene()
    {
        
        menuCanvas.SetActive(false);
        levelCanvas.SetActive(true);

        audioBGManager.levelBackgroundAudioPlay();
        SetCoinsCanvas();
    }

    public void IntoMenuScene()
    {
        GameOverCanvas.SetActive(false);
        levelCanvas.SetActive(false);

        menuCanvas.SetActive(true);
        audioBGManager.menuBackgroundAudioPlay();
    }

    internal void IntoGameOverScene()
    {
        levelCanvas.SetActive(false);
        GameOverCanvas.SetActive(true);
        audioBGManager.gameOverBackgroundAudioPlay();
    }

    #region Canvas control

    public void SetMaxHealtBar(int health) {
        levelCanvas.GetComponentInChildren<UnityEngine.UI.Slider>().maxValue = health;
        levelCanvas.GetComponentInChildren<UnityEngine.UI.Slider>().value = health;
    }

    public void SetHealtBar(int health)
    {
        levelCanvas.GetComponentInChildren<UnityEngine.UI.Slider>().value = health;
    }

    public void SetEnemiesCanvas(int enemiesAlive)
    {
        enemiesInCanvas.text = "Enemies: " + enemiesAlive;
    }

    public void SetPlayerStatCanvas(float m_speed, float m_timeToShoot) 
    {
        playerStats.text = "Damage: " + m_playerController.m_damage + "\nAttack S: " + m_timeToShoot + "\nMove S: " + m_speed;
    }

    public void SetCoinsCanvas()
    {
        coinsCanvas.text = "x"+coins;
    }

    public void PauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void ReturnToMenu()
    {
        ResumeGame();
        LoadMenuScene();
    }

    public void CountDownNextLevel(int t)
    {
        CountDownCanvas.gameObject.SetActive(true);
        if (t > 0)
        {
            CountDownCanvas.text = $"Next Level\nin\n{t}";
        }
        else 
        { 
            CountDownCanvas.text = "Level START!";
        }
    }

    internal void DisableCountDown()
    {
        CountDownCanvas.gameObject.SetActive(false);
    }

    #endregion

    // OPTIONS CANVAS CONTROL

    public void ActiveCanvasOptions()
    {
        pauseMenu.SetActive(false);
        optionsCanvas.SetActive(true);
    }

    public void DisableCanvasOptions()
    {
        if (gameIsPaused)
            pauseMenu.SetActive(true);

        optionsCanvas.SetActive(false);
    }
    
}
