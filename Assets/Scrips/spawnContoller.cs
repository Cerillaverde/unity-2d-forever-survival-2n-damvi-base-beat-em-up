
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class spawnContoller : MonoBehaviour
{

    [SerializeField]
    private GameObject m_enemie;

    private List<Vector3> m_spawnList = new List<Vector3>();


    private void Start()
    {
        LevelManager.Instance.OnSucceit += spawnOn;

        m_spawnList.Add(new Vector3(-22, 4, 0));
        m_spawnList.Add(new Vector3(-17, -13, 0));
        m_spawnList.Add(new Vector3(10, -12, 0));
        m_spawnList.Add(new Vector3(3, 4, 0));
    }

    public void spawnOn()
    {
        StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        LevelManager.Instance.enemiesAlive = GameManager.Instance.level * 5;
        int enemyNumber = LevelManager.Instance.enemiesAlive;
        GameManager.Instance.SetEnemiesCanvas(GameManager.Instance.level * 5);
        //GameManager.Instance.SetEnemiesCanvas(3);
        //LevelManager.Instance.enemiesAlive = 3;
        for (int i = 0; i < enemyNumber; i++)
        {
            GameObject newMob = Instantiate(m_enemie);
            NavMeshAgent mobNav = newMob.GetComponent<NavMeshAgent>();

            mobNav.Warp(m_spawnList[Random.Range(0, m_spawnList.Count)]);

            yield return new WaitForSeconds(Random.Range(0.6f, 1f)); 
        }
    }

    private void OnDestroy()
    {
        LevelManager.Instance.OnSucceit -= spawnOn;
    }

}
