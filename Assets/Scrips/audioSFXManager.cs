using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioSFXManager : MonoBehaviour
{

    [SerializeField] private AudioClip playerHit;
    [SerializeField] private AudioClip enemyHit;
    [SerializeField] private AudioClip enemyDie;
    [SerializeField] private AudioClip shoot;

    [SerializeField] private SOGeneralStats SOPersistent;

    private AudioSource audioSource;

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void setVolume()
    {
        audioSource.volume = SOPersistent.audioSFXVolume;
    }

    public void playerHitAudioPlay()
    {
        audioSource.clip = playerHit;
        audioSource.Play();
    }

    public void enemyHitAudioPlay()
    {
        audioSource.clip = enemyHit;
        audioSource.Play();
    }

    public void enemyDieAudioPlay()
    {
        audioSource.clip = enemyDie;
        audioSource.Play();
    }

    public void shootAudioPlay()
    {
        audioSource.clip = shoot;
        audioSource.Play();
    }

}
