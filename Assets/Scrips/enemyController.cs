using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class enemyController : MonoBehaviour
{

    [HideInInspector] public playerController player;

    [SerializeField] private GameEvent onDie;
    [SerializeField] private SOEnemy[] enemy;
    [SerializeField] private ParticleSystem explosion;

    NavMeshAgent m_agent;

    // Stats enemy
    private float speed;
    private int maxHP;
    private int actualHP;
    private int damage;

    // Memory
    private bool canMove;
    private bool canAttack;


    void Start()
    {
        player = GameManager.Instance.GetPlayerController();

        canMove = true;
        canAttack = true;

        int rnd = Random.Range(0, enemy.Length);
        SOEnemy m_enemyChoose = enemy[rnd];

        GetComponent<SpriteRenderer>().sprite = m_enemyChoose.m_sprite;
        maxHP = m_enemyChoose.m_maxHP + GameManager.Instance.level;
        actualHP = maxHP;
        speed = m_enemyChoose.m_moveSpeed;
        damage = m_enemyChoose.m_damage;

        m_agent = GetComponent<NavMeshAgent>();
        m_agent.updateRotation = false;
        m_agent.updateUpAxis = false;
        m_agent.speed = speed;

        switch (rnd)
        {
            case 0:
                m_agent.avoidancePriority = 3; break;
            case 1:
                m_agent.avoidancePriority = 2; break;
            case 2:
                m_agent.avoidancePriority = 1; break;
            case 3:
                m_agent.avoidancePriority = 4; break;
        }

    }

    void Update()
    {
        if (canMove)
            MoveToPlayer();

        float distance = Vector2.Distance(player.transform.position, this.transform.position);

        if (distance < 0.8f && canAttack)
            PlayerHit();

    }

    void MoveToPlayer()
    {
        m_agent.SetDestination(player.transform.position);
        if (m_agent.speed == 0)
            m_agent.speed = speed;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("shoot"))
        {
            actualHP -= player.m_damage;
            collision.gameObject.SetActive(false);
            if (actualHP <= 0)
            {
                GameManager.Instance.audioSFXManager.enemyDieAudioPlay();
                explosion.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, explosion.transform.position.z);
                explosion.Play();
                onDie.Raise();
                Destroy(gameObject);
            }
            else
            {
                canMove = false;
                m_agent.SetDestination(transform.position);
                m_agent.speed = 0;
                GetComponent<SpriteRenderer>().color = Color.red;
                StartCoroutine(EnemyHitCooldown());
             }
        }
    }


    private void PlayerHit()
    {
        canAttack = false;
        canMove = false;
        player.GetComponent<SpriteRenderer>().color = Color.red;
        player.OnHit(damage);
        StartCoroutine(PlayerHitCooldown());
    }

    IEnumerator PlayerHitCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        canAttack = true; 
        canMove = true;
        player.GetComponent<SpriteRenderer>().color = Color.white;
    }

    IEnumerator EnemyHitCooldown()
    {
        yield return new WaitForSeconds(0.3f);
        canMove = true;
        GetComponent<SpriteRenderer>().color = Color.white;
    }


}
