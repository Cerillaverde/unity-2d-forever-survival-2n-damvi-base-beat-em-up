using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public static LevelManager Instance = null;

    [SerializeField] private GameObject spawner;
    [SerializeField] private GameObject powerUps;



    [HideInInspector]
    public int enemiesAlive;

    public delegate void EventSucceit();
    public event EventSucceit OnSucceit;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GameManager.Instance.level = 1;
        NextLevel();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.Instance.gamePause)
            {
                GameManager.Instance.ResumeGame();
            }
            else
            {
                GameManager.Instance.PauseGame();
            }
        }
    }

    public void OnEnemyDie()
    {
        enemiesAlive--;
        GameManager.Instance.coins += 5;
        GameManager.Instance.SetCoinsCanvas();
        GameManager.Instance.SetEnemiesCanvas(enemiesAlive);

        if (enemiesAlive <= 0)
        {
            EndLevel();
        }
    }

    public void EndLevel()
    {
        powerUps.gameObject.SetActive(true);
        GameManager.Instance.level++;
        StartCoroutine(EndLevelCountdown());
    }

    public void StopEndLevel()
    {
        StopAllCoroutines();
        NextLevel();
    }

    IEnumerator EndLevelCountdown()
    {
        yield return new WaitForSeconds(20);
        NextLevel();
    }

    public void StartLevel()
    {
        // Delegado a Spawn
        OnSucceit?.Invoke();
    }

    public void NextLevel()
    {
        powerUps.gameObject.SetActive(false);

        StartCoroutine(LvlStart(3));
    }

    IEnumerator LvlStart(int t)
    {
        if (t < 0)
        {
            StartLevel();
            GameManager.Instance.DisableCountDown();
        }
        else
        {
            GameManager.Instance.CountDownNextLevel(t);
            yield return new WaitForSeconds(1);
            StartCoroutine(LvlStart(t - 1));
        }
        
        
    }

}
