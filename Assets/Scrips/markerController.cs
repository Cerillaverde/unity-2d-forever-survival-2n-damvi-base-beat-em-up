using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class markerController : MonoBehaviour
{

    private Vector3 m_position;

    void Update()
    {
        m_position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 rotation = m_position - transform.position;
        float rot = Mathf.Atan2(rotation.y, rotation.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rot);
    }


}
