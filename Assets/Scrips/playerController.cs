using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.EventSystems.EventTrigger;

public class playerController : MonoBehaviour
{

    [SerializeField] private GameObject poolShoot;

    [SerializeField] private SOPlayer[] playerSelected;

    private PlayerInput playerInput;
    private Vector2 movement;


    // STATS SO
    private SOPlayer player;
    private int maxHp;
    private int actualHp;
    private float speed;
    private float timeToShoot;
    private int damage;
    public int m_damage => damage;

    // memory
    private bool inmune = false;
    private bool shoot = true;

    void Start()
    {

        player = playerSelected[GameManager.Instance.playerSelected];
        playerInput = GetComponent<PlayerInput>();

        // STATS
        GetComponent<SpriteRenderer>().sprite = player.m_sprite;
        maxHp = player.m_maxHp;
        actualHp = maxHp;
        damage = player.m_damage;
        speed = player.m_speed;
        timeToShoot = player.m_timeToShoot;

        GameManager.Instance.SetPlayer(this);
        GameManager.Instance.SetMaxHealtBar(maxHp);
        GameManager.Instance.SetPlayerStatCanvas(speed, timeToShoot);
    }

    void Update()
    {

        movement = playerInput.actions["Move"].ReadValue<Vector2>();

    }

    private void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = movement.normalized * speed;
    }

    public void funcShoot()
    {
        if (shoot)
        {
            GameManager.Instance.audioSFXManager.shootAudioPlay();
            for (int i = 0; i < poolShoot.transform.childCount; i++)
            {
                Transform tActual = poolShoot.transform.GetChild(i);
                if (!tActual.gameObject.activeSelf)
                {
                    tActual.gameObject.SetActive(true);
                    tActual.transform.position = transform.position;
                    Vector2 direction = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - this.transform.position.x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y - this.transform.position.y).normalized;
                    tActual.gameObject.GetComponent<Rigidbody2D>().velocity = direction * 10;
                    break;
                }
            }
            StartCoroutine(ShootCoolDown(timeToShoot));
        }
    }

    public void OnHit(int damage)
    {
        if (!inmune)
        {
            actualHp -= damage;
            if (!GameManager.Instance.audioSFXManager.GetComponent<AudioSource>().isPlaying)
                GameManager.Instance.audioSFXManager.playerHitAudioPlay();

            GameManager.Instance.SetHealtBar(actualHp);
            if (actualHp <= 0)
            {
                GameManager.Instance.LoadGameOverScene();
            }
            inmune = true;
        }
        else
        {
            StartCoroutine(SetInmune());
        }

    }

    IEnumerator SetInmune()
    {
        yield return new WaitForSeconds(0.1f);
        inmune = false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            speed += collision.gameObject.GetComponent<powerUp>().m_speed;
            damage += collision.gameObject.GetComponent<powerUp>().m_damage;
            timeToShoot -= collision.gameObject.GetComponent<powerUp>().m_attackSpeed;

            GameManager.Instance.SetPlayerStatCanvas(speed, timeToShoot);

            if (timeToShoot < 0.2f)
                timeToShoot = 0.2f;

            LevelManager.Instance.StopEndLevel();
        }
    }

    IEnumerator ShootCoolDown(float timeToShoot)
    {
        shoot = false;
        yield return new WaitForSeconds(timeToShoot);
        shoot = true;
    }
}
