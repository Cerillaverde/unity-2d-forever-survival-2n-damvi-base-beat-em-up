using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SOEnemy", menuName = "Scriptable Objects/SO Enemy")]
public class SOEnemy : ScriptableObject
{

    public Sprite m_sprite;
    
    public int m_maxHP;
    public float m_moveSpeed;
    public int m_damage;

}

