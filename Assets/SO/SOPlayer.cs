using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SOPlayer", menuName = "Scriptable Objects/SO Player")]
public class SOPlayer : ScriptableObject
{

    public Sprite m_sprite;

    public int m_maxHp;
    public float m_speed;
    public int m_damage;
    public float m_timeToShoot;

}
