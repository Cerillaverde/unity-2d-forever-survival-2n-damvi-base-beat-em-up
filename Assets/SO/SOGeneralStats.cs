using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SOGeneralStats", menuName = "Scriptable Objects/SO General Stats")]

public class SOGeneralStats : ScriptableObject
{

    public int coins;
    public int playerSelected;

    public float audioBGVolume;
    public float audioSFXVolume;


}
